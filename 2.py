import os
import datetime
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains

# datalist = ['Khakhar', 'Nikunj Dilipkumar', 'arpithgre@gmail.com','K4833461', '9739996794']
# datalist = ['JOY', 'ANEL ', 'lincyjoy1015s@yopmail.com', 'T4857335', '9605796491']
datalist = ['Anil Kumar', 'Gopinath Shenoy', 'shenoya@yopmail.com', 'T1822445', '9496567170']

driver = webdriver.Chrome('chromedriver.exe')



while datetime.datetime.now().minute < 59:
	pass

while datetime.datetime.now().second < 47:
	pass

# the following is for ATTESTATION & TESTING ONLY
# driver.get("https://service2.diplo.de/rktermin/extern/appointment_showMonth.do?locationCode=banga&realmId=208&categoryId=338&dateStr=21.08.2019")

# the following is the original
driver.get("https://service2.diplo.de/rktermin/extern/appointment_showMonth.do?locationCode=banga&realmId=210&categoryId=337&dateStr=30.09.2019")
driver.implicitly_wait(1)
flag = True

while True:
	driver.implicitly_wait(0.01)
	if driver.find_elements_by_xpath("//*[contains(text(), 'Repeat email:')]") and driver.find_elements_by_xpath("//*[contains(text(), 'Phone Number')]"):
		driver.find_element_by_id("appointment_newAppointmentForm_lastname").send_keys(datalist[0])
		driver.find_element_by_id("appointment_newAppointmentForm_firstname").send_keys(datalist[1])
		driver.find_element_by_id("appointment_newAppointmentForm_email").send_keys(datalist[2])
		driver.find_element_by_id("appointment_newAppointmentForm_emailrepeat").send_keys(datalist[2])
		driver.find_element_by_id("appointment_newAppointmentForm_fields_0__content").send_keys(datalist[3])
		driver.find_element_by_id("appointment_newAppointmentForm_fields_1__content").send_keys(datalist[4])
		actions = ActionChains(driver)
		actions.send_keys(Keys.TAB * 2)
		actions.perform()
		break
	elif driver.find_elements_by_xpath("//*[contains(text(), 'Please enter here the text you see in the picture above:')]") and not driver.find_elements_by_xpath("//*[contains(text(), 'Repeat email:')]"):
		# driver.implicitly_wait(0.1)
		try:
			WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, "appointment_captcha_month_captchaText"))).click()
		except:
			continue
		continue
	elif driver.find_elements_by_xpath("//*[contains(text(), 'Appointments are available')]") and not driver.find_elements_by_xpath("//*[contains(text(), 'Repeat email:')]"): 
		# if flag:
		# 	# WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="content"]/div[1]/h2[2]/a[2]/img'))).click()
		# 	# WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="content"]/div[1]/h2[2]/a[2]/img'))).click()
		# 	# WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="content"]/div[1]/h2[2]/a[2]/img'))).click()
		# 	flag = False
		# 	continue		
		driver.find_elements_by_xpath("//*[contains(text(), 'Appointments are available')]")[-1].click()
		# driver.find_elements_by_xpath("//*[contains(text(), 'Book this appointment')]")[-1].click()
	elif driver.find_elements_by_xpath("//*[contains(text(), 'Book this appointment')]") and not driver.find_elements_by_xpath("//*[contains(text(), 'Repeat email:')]"): 
		driver.find_elements_by_xpath("//*[contains(text(), 'Book this appointment')]")[-3].click()

	elif driver.find_elements_by_xpath("//*[contains(text(), 'Unfortunately, there are no')]") and not driver.find_elements_by_xpath("//*[contains(text(), 'Repeat email:')]"):
		# //*[@id="content"]/div[1]/h2[2]/a[1]
		WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="content"]/div[1]/h2[2]/a[2]/img'))).click()
		time.sleep(0.4)
		continue
